$(function () {

    var section = $('section');
    var links = $('.side-menu li');
    var bodyHtml = $('html, body');
    var sq = $('#sq');
    var sqPos = [0, -172, -354, -519];
    var step = 0;
    var lastStep = 0;

    skrollr.init({
        smoothScrolling: false,
        forceHeight: false,
        beforerender: function (skrollr) {

            if (skrollr.curTop > lastStep + 200) {
                step < sqPos.length -1 ? step++ : step = 0;
                sq.css('background-position', sqPos[step] + 'px 0px');
                lastStep = skrollr.curTop;
            } else if (skrollr.curTop < lastStep - 200) {
                step < sqPos.length -1 ? step++ : step = 0;
                sq.css('background-position', sqPos[step] + 'px 0px');
                lastStep = skrollr.curTop;
            }
        }
    });

    section.waypoint(function (direction) {
        var that = $(this);
        var dataSection = that.attr('data-section');

        if (direction == 'down') {
            $('.side-menu li[data-section="' + dataSection + '"]').addClass('active').siblings().removeClass('active');
        } else {
            var newSection = parseInt(dataSection) - 1;
            $('.side-menu li[data-section="' + newSection + '"]').addClass('active').siblings().removeClass('active');
        }
    });

    links
        .on('click', function () {
            scrollTo($(this).attr('data-section'));
        })
        .on('mouseover', function () {
            $(this).animate({
                right: 0
            }, 500)
        })
        .on('mouseleave', function() {
            $(this).stop( true, true ).animate({
                right: -74
            }, 10)
        });

    function scrollTo (toSection) {
        bodyHtml.animate({
            scrollTop: $('section[data-section="' + toSection + '"]').offset().top
        }, 750, 'easeInOutCubic');
    }


    var maps = {
        1: function () {
            var testMap = new GMaps({
                div: '#testMap',
                lat: 44.9311114,
                lng: 34.0859371
            });
            testMap.addMarkers([
                {
                    lat: 44.9311115,
                    lng: 34.0859371,
                    title: 'Horses',
                    draggable: true,
                    icon: 'img/nut.png'
                },
                {
                    lat: 44.9447923,
                    lng: 34.1032887,
                    title: '2',
                    draggable: true,
                    icon: 'img/nut.png'
                },
                {
                    lat: 44.9444885,
                    lng: 34.0850926,
                    title: '3',
                    draggable: true,
                    icon: 'img/nut.png'
                }
            ]);
            testMap.fitZoom();
        },
        2: function () {
            var testMap2 = new GMaps({
                div: '#testMap2',
                lat: 44.9311114,
                lng: -122.322453,
                zoom: 8
            });
        },
        3: function () {
            var testMap3 = new GMaps({
                div: '#testMap3',
                lat: 39.732630,
                lng: -104.973785,
                zoom: 8
            });
        },
        4: function () {
            var testMap4 = new GMaps({
                div: '#testMap4',
                lat: 39.732630,
                lng: -104.973785
            });
        }
    };

    maps[1]();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
        var id = $(this).attr('data-map');
        maps[id]();
    });
});
